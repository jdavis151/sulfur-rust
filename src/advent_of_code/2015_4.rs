use md5;

fn main() {
    let input: &str = "ckczppom";
    println!("{}", advent_of_code_2015_4a(input));
    println!("{}", advent_of_code_2015_4b(input));
}

fn advent_of_code_2015_4a(input: &str) -> i32 {
    let mut searching = true;
    let mut deci = 0;

    while searching {
        deci += 1;

        let key = format!("{}{}", input, deci);

        let digest: String = format!("{:x}", md5::compute(key));
        
        if &digest[..5] == "00000" {
            searching = false;
        }
    }

    return deci;
}

fn advent_of_code_2015_4b(input: &str) -> i32 {
    let mut searching = true;
    let mut deci = 0;

    while searching {
        deci += 1;

        let key = format!("{}{}", input, deci);

        let digest: String = format!("{:x}", md5::compute(key));
        
        if &digest[..6] == "000000" {
            searching = false;
        }
    }

    return deci;
}

